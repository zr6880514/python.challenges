def invest(amount, rate, years):
    '''Prints amount of investment to 2 dec. places, at the end of each year'''
    for current_year in range(years):
        amount = amount * (1 + rate)
        print(f"year {current_year + 1}: ${amount:.2f}")

amount = float(input("Enter the initial amount: "))
rate = float(input("Enter annual percentage rate: "))               
years = int(input("Enter number of years: "))

invest(amount, rate, years)
