import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}

state, capital = random.choice(list(capitals_dict.items()))

print(f"Name of the state: {state}.")

capital_guess = ""

while capital_guess != capital.lower() and capital_guess != "exit":
    capital_guess = input(f"What's the capital of {state}? \
Guess the capital or type 'exit' if you surrender: ").lower()
    if capital_guess == capital.lower():
        print("Correct")
    elif capital_guess == "exit":
        print(f"Correct answer is: {capital}")
        print("Goodbye")
