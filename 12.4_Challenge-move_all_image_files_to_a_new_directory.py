import pathlib

current_dir = pathlib.Path.cwd()
doc_dir = current_dir.parent / "ch12-file-input-and-output" / "practice_files" / "documents"
practise_dir = doc_dir.parent

images_dir = practise_dir / "images"
images_dir.mkdir(exist_ok=True)

image_files = []
image_files.extend(doc_dir.rglob("*.gif"))
image_files.extend(doc_dir.rglob("*.jpg"))
image_files.extend(doc_dir.rglob("*.png"))

for file in image_files:
    file.replace(images_dir / file.name)

