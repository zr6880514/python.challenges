import random

def flip():
    """Randomly return 'heads' or 'tails'."""
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"

trial_amount = 10_000
sequence_sum = 0

for trial in range(trial_amount):
    current_flip = flip()
    flips_in_sequence = 0
    
    if current_flip == "heads":
        while current_flip == "heads":
            current_flip = flip()
            flips_in_sequence = flips_in_sequence + 1
    else:    
        while current_flip == "tails":
            current_flip = flip()
            flips_in_sequence = flips_in_sequence + 1
    flips_in_sequence = flips_in_sequence + 1
    sequence_sum = sequence_sum + flips_in_sequence

average_flips = sequence_sum / trial_amount
print(f"Average number of flips per trial is {average_flips:.3f}")
