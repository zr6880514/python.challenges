from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

class PdfFileSplitter:
    """
    Reads a PDF from an existing PdfFileReader instance
    and splits PDF into two new PDFs. Works with PyPdf==1.26.0
    """
    
    def __init__(self, input_path):
        self.input_path = Path(input_path)
        self.input_pdf = PdfFileReader(str(self.input_path))

    def split(self, *, break_point):
        """
        Break_point represent the page number at which to split the PDF
        """
        self.break_point = break_point
        self.writer1 = PdfFileWriter()
        for page in self.input_pdf.pages[:break_point]:
            self.writer1.addPage(page)

        self.writer2 = PdfFileWriter()
        for page in self.input_pdf.pages[break_point:]:
            self.writer2.addPage(page)

    def write(self, filename):
        """
        Saves two PDFs to paths 'filename + "_1.pdf"' and 'filename + "_2.pdf"'
        """
        
        part1_path = Path.cwd() / (filename + "_1.pdf")
        with part1_path.open(mode="wb") as output_file:
            self.writer1.write(output_file)

        part2_path = Path.cwd() / (filename + "_2.pdf")
        with part2_path.open(mode="wb") as output_file:
            self.writer2.write(output_file)


# Checking pdf_splitter functionality
pdf_splitter = PdfFileSplitter("Pride_and_Prejudice.pdf")
pdf_splitter.split(break_point=150)
pdf_splitter.write("proba")
