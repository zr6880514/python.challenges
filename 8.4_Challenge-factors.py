num = int(input("Enter a positive integer: "))

if num > 0:
    i = 1
    while i <= num:
        if num % i == 0:
            print(f"{i} is a factor of {num}")
        i = i + 1
    
else:
    print("That's not a positive integer")
