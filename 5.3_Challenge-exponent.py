base = input("Enter a base: ")
exponent = input("Enter the exponent: ")

base = float(base)
exponent = float(exponent)

result = base ** exponent

print(f"{base} to the power of {exponent} = {result}")
