def convert_cel_to_far(celsius):
    return celsius * 9 / 5 + 32

def convert_far_to_cel(fahrenheit):
    return (fahrenheit - 32) * 5 / 9

fahrenheit_temp = input("Enter a temperature in degrees F: ")
fahrenheit_temp = float(fahrenheit_temp)
to_celsius = convert_far_to_cel(fahrenheit_temp)
print(f"{fahrenheit_temp:.0f} degrees F = {to_celsius:.2f} degrees C")

print()

celsius_temp = input("Enter a temperature in degrees C: ")
celsius_temp = float(celsius_temp)
to_fahrenheit = convert_cel_to_far(celsius_temp)
print(f"{celsius_temp:.0f} degrees C = {to_fahrenheit:.2f} degrees F")
