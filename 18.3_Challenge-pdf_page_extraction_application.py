# EasyGUI application for extracting pages from a PDF file
# PyPDF2==1.26.0
import easygui as gui
from PyPDF2 import PdfFileReader, PdfFileWriter

input_path = gui.fileopenbox(
    title="Select a PDF to extract pages from...",
    default="*.pdf"
    )

if input_path is None:
    exit()

input_file = PdfFileReader(input_path)
pdf_pages_num = input_file.getNumPages()

while True:
    try:
        start_page = gui.enterbox(
                msg="Enter starting page number:",
                title="Starting page number"
                )
        if start_page is None:
            exit()
        start_page_num = int(start_page)
        if 0 < start_page_num < pdf_pages_num:
            break
        else:
            gui.msgbox(
                msg=f"Entry is invalid. Page number should be positive integer and less than number of pdf pages: {pdf_pages_num}",
                title="Information"
                )
        
    except ValueError:
        gui.msgbox(
            msg="Entry is invalid. Provide a number.",
                title="Information"
                )

while True:
    try:
        end_page = gui.enterbox(
                msg="Enter ending page number:",
                title="Ending page number"
                )
        if end_page is None:
            exit()
            
        end_page_num = int(end_page)
        if start_page_num < end_page_num <= pdf_pages_num:
            break
        else:
            gui.msgbox(
                msg=f"Entry is invalid. Page number should be greater than start page: {start_page_num} and less or equal number of pdf pages: {pdf_pages_num}",
                title="Information"
                )
        
    except ValueError:
        gui.msgbox(
            msg="Entry is invalid. Provide a number.",
            title="Information"
            )

while True:
    output_path = gui.filesavebox(
        title="Save an extracted PDF as...",
        default="*.pdf"
    )
    if output_path is None:
        exit()
        
    if input_path != output_path:
        break
    else:
        gui.msgbox(
            msg="Can't overwrite input file!",
            )

writer = PdfFileWriter()
for page in input_file.pages[(start_page_num - 1):end_page_num]:
    writer.addPage(page)
    
with open(output_path, mode="wb") as output_file:
    writer.write(output_file)
    
gui.msgbox(msg="File saved successfully!", title="Success!")
