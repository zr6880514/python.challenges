import tkinter as tk
from tkinter.filedialog import asksaveasfilename
import random

def separate_words_from_entry(entry):
    """Split comma-separated string into a list"""
    word_list = entry.split(",")
    return(word_list)

def add_words_from_entry(word_entries):
    """Split entries into word lists and update word_entries"""
    for word_type in word_entries:
       entry = word_entries[word_type]["entry"].get()
       word_list = separate_words_from_entry(entry)
       word_entries[word_type]["words"] = list(filter(None, word_list))

def word_count_correct(word_entries):
    """Check if the provided word counts meet the required numbers"""
    for word_type in word_entries:
        provided_words_num = len(word_entries[word_type]["words"])
        required_words_num = word_entries[word_type]["req_word_num"]
        if provided_words_num < required_words_num:
            lbl_poem.config(text=
                f"""ERROR:
    
    To few words provided in {word_type} entry.
    There should be {required_words_num} words.
    You entered only {provided_words_num}.""")
            return False
    return True

def unique_input_words(word_entries):
    """Check if all input words are unique for each word type"""
    for word_type in word_entries:
        word_list = word_entries[word_type]["words"]
        for word in word_list:
            if word_list.count(word) > 1:
                lbl_poem.config(text=
                f"""ERROR:
    
    Word '{word}' typed more than once.""")
                return False
    return True
    
def assign_words_from_entries(word_entries, poem_words):
    """Randomly assign words from word_entries to poem placeholders"""
    for element in poem_words:
        available_words = word_entries[poem_words[element]["type"]]["words"]
        poem_words[element]["word"] = random.choice(available_words)
        chosen_word = poem_words[element]["word"]
        available_words.remove(chosen_word)
        
def indefinite_article(adjective):
    '''"An" if following adjective begins with vowel, otherwise "A"'''
    vowels_list = list("aeiou")
    for letter in vowels_list:
        if adjective[0].lower() == letter:
            return "An"
    return "A"


def poem_from_template(poem_dict):
    """Generate a poem using the provided word dictionary"""
    poem = f"""{indefinite_article(poem_dict["adj1"]["word"])} {poem_dict["adj1"]["word"]} {poem_dict["noun1"]["word"]}

A {poem_dict["adj1"]["word"]} {poem_dict["noun1"]["word"]} {poem_dict["verb1"]["word"]} {poem_dict["prep1"]["word"]} the {poem_dict["adj2"]["word"]} {poem_dict["noun2"]["word"]}
{poem_dict["adverb1"]["word"]}, the {poem_dict["noun1"]["word"]} {poem_dict["verb2"]["word"]}
the {poem_dict["noun2"]["word"]} {poem_dict["verb3"]["word"]} {poem_dict["prep2"]["word"]} a {poem_dict["adj3"]["word"]} {poem_dict["noun3"]["word"]}
"""
    return poem

def generate_poem():
    """Generate and display a poem after validating the input"""
    add_words_from_entry(word_entries)
    if word_count_correct(word_entries) and unique_input_words(word_entries):
        assign_words_from_entries(word_entries, poem_words)
        lbl_poem.config(text=poem_from_template(poem_words))
       
def save_file():
    """Save text to a new file."""

    filepath = asksaveasfilename(
        defaultextension="txt",
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")]
    )

    if not filepath:
        return

    with open(filepath, "w") as output_file:
        text = lbl_poem.cget("text")
        output_file.write(text)

    window.title(f"Make your own poem! - {filepath}")

window = tk.Tk()
window.title("Make your own poem!")

for i in range(4):
    window.rowconfigure(i, weight=1)

window.columnconfigure(0, weight=1)

lbl_word_input = tk.Label(
    master=window,
    text="Enter your favorite words, separated by commas:",
    height=3
    )
lbl_word_input.grid(row=0, column=0, sticky="ew")

fr_word_input = tk.Frame(window)

word_entries = {
    "Nouns": {
        "row_num": 0,
        "entry": "",
        "req_word_num": 3,
        "words": []
        },
    "Verbs": {
        "row_num": 1,
        "entry": "",
        "req_word_num": 3,
        "words": []
        },
    "Adjectives": {
        "row_num": 2,
        "entry": "",
        "req_word_num": 3,
        "words": []
        },
    "Prepositions": {
        "row_num": 3,
        "entry": "",
        "req_word_num": 3,
        "words": []
        },
    "Adverbs": {
        "row_num": 4,
        "entry": "",
        "req_word_num": 1,
        "words": []
        }
    }

poem_words = {
    "noun1": {
        "type": "Nouns",
        "word": ""
        },
    "noun2": {
        "type": "Nouns",
        "word": ""
        },
    "noun3": {
        "type": "Nouns",
        "word": ""
        },
    "verb1": {
        "type": "Verbs",
        "word": ""
        },
    "verb2": {
        "type": "Verbs",
        "word": ""
        },
    "verb3": {
        "type": "Verbs",
        "word": ""
        },
    "adj1": {
        "type": "Adjectives",
        "word": ""
        },
    "adj2": {
        "type": "Adjectives",
        "word": ""
        },
    "adj3": {
        "type": "Adjectives",
        "word": ""
        },
    "prep1": {
        "type": "Prepositions",
        "word": ""
        },
    "prep2": {
        "type": "Prepositions",
        "word": ""
        },
    "adverb1": {
        "type": "Adverbs",
        "word": ""
        },
    }

for word_type in word_entries:
    label = tk.Label(
        master=fr_word_input,
        text=word_type + ":"
        )
    label.grid(row=word_entries[word_type]["row_num"], column=0, sticky="e")
    entry = tk.Entry(
        master=fr_word_input,
        width=80
        )
    entry.grid(row=word_entries[word_type]["row_num"], column=1)
    word_entries[word_type]["entry"] = entry
    
fr_word_input.grid(row=1, column=0, padx=5, pady=5)

btn_generate = tk.Button(master=window, text="Generate", command=generate_poem)
btn_generate.grid(row=2, column=0, pady=10)

fr_poem = tk.Frame(master=window, relief=tk.GROOVE, borderwidth=5)
fr_poem.columnconfigure(0, weight=1)

lbl_poem = tk.Label(
    master=fr_poem,
    text="There will be your poem soon:",
    height=9
    )
lbl_poem.grid(row=0, column=0, sticky="ew")

btn_save = tk.Button(master=fr_poem, text="Save to file", command=save_file)
btn_save.grid(row=1, column=0, pady=10)

fr_poem.grid(row=3, column=0, padx=5, pady=5, sticky="ew")

window.mainloop()
