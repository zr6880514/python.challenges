import csv
from pathlib import Path

input_csv_path = Path.cwd() / "scores.csv"

all_scores_list = []
with input_csv_path.open(mode="r", encoding="utf-8", newline="") as file:
    reader = csv.DictReader(file)
    for row in reader:
        all_scores_list.append(row)

high_scores_dict = {}
for player_entry in all_scores_list:
    name = player_entry["name"]
    score = player_entry["score"]
    if name not in high_scores_dict or score > high_scores_dict[name]["high_score"]:
        high_scores_dict[name] = {"name": name, "high_score": score}
    
high_scores_list = list(high_scores_dict.values())

output_csv_path = Path.cwd() / "high_scores.csv"

with output_csv_path.open(mode="w", encoding="utf-8", newline="") as file:
    writer = csv.DictWriter(file, fieldnames=high_scores_list[0].keys())
    writer.writeheader()
    writer.writerows(high_scores_list)
