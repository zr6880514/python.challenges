def switch_hat(cat):
    if cat == "off":
        return "on"
    elif cat == "on":
        return "off"

cats_number = 100
hat_on_cat = ["off" for i in range(cats_number)]

stage = 1

for stage_number in range(cats_number):
    for cat in range(cats_number):
        if (cat + 1) % (stage_number + 1) == 0:
            hat_on_cat[cat] = switch_hat(hat_on_cat[cat])

print("At the end cats with number: ", end = '')
for cat in range(cats_number):
    if hat_on_cat[cat] == "on":
        print(f"{cat + 1} ", end = '')
print("have a hat on.")
