from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

pdf_path = (
    Path.home() /
    "python-basics-exercises" /
    "ch14-interact-with-pdf-files" /
    "practice_files" /
    "scrambled.pdf"
    )

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

# Firstly, create dict with page content in key, page object in value
pages_dict = {}
for page in pdf_reader.pages:
    page_num = str(page.extractText())
    pages_dict[page_num] = page

# Now, sort dictionary by keys, and create list containing values of sorted dict
sorted_keys = sorted(pages_dict.keys())
sorted_pages_dict = {key: pages_dict[key] for key in sorted_keys}
sorted_pages_list = list(sorted_pages_dict.values())


for page in sorted_pages_list:
    page_rotation = page["/Rotate"]
    if page_rotation != 0:
        page.rotateCounterClockwise(page_rotation)
    pdf_writer.addPage(page)


output_path = Path.home() / "unscrambled.pdf"
with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)

