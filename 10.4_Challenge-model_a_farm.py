class Animal:   
    def __init__(self, name, age, hours_slept):
        self.name = name
        self.age = age
        self.hours_slept = hours_slept
        self_sleeping_hours = 1

    def put_to_sleep(self, sleep_time):
        self.hours_slept += sleep_time

    def is_rested(self):
        if self.hours_slept >= self.sleeping_hours:
            return f"{self.name} is rested."
        else:
            return f"{self.name} need to rest. \
Give {self.name}'s {self.sleeping_hours - self.hours_slept:.2f} hours of sleep."

    def speak(self, sound):
        return f"{self.name} says {sound}."
    
    def __str__(self):
        return f"{self.name} is {self.age} years old."
    
class Cow(Animal):
    sleeping_hours = 4
    
    def speak(self, sound = "Muu"):
        return super().speak(sound)
    
class Horse(Animal):
    sleeping_hours = 2.9
    def speak(self, sound = "Whinny"):
        return super().speak(sound)

class Goat(Animal):
    sleeping_hours = 5
    def speak(self, sound = "Baa"):
        return super().speak(sound)

class Dog(Animal):
    sleeping_hours = 12
    def speak(self, sound = "Woof"):
        return super().speak(sound)

milka = Cow("Milka", 6, 5)
ben = Horse("Ben", 9, 2)
billy = Goat("Billy", 3, 1)
rex = Dog("Rex", 4, 7)

rex.put_to_sleep(5)

creatures = [milka, ben, billy, rex]

for animal in creatures:
    print(animal)
    print(animal.is_rested())
