universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_status(universities):
    """List of all student enrollment values, list of all tuition fees"""
    enrolled_students = []
    tuition_fees = []
    for row_num in range(len(universities)):
        enrolled_students.append(universities[row_num][1])
        tuition_fees.append(universities[row_num][2])
    return enrolled_students, tuition_fees

def mean(num_list):
    return sum(num_list) / len(num_list)

def median(num_list):
    """For even list: mean of two middle elements, for odd list: middle element."""
    num_list.sort()
    if len(num_list) % 2 == 0:
        lower_even_index = int((len(num_list) - 1) / 2)
        upper_even_index = lower_even_index + 1
        return (num_list[lower_even_index] + num_list[upper_even_index]) / 2
    else:
        middle_odd_index = int(len(num_list) / 2)
        return num_list[middle_odd_index]
    
total_students_num = sum(enrollment_status(universities)[0])
total_tuition = sum(enrollment_status(universities)[1])
mean_students_num = mean(enrollment_status(universities)[0])
median_students_num = median(enrollment_status(universities)[0])
mean_tuition_num = mean(enrollment_status(universities)[1])
median_tuition_num = median(enrollment_status(universities)[1])

for i in range(30):
    print("*", end = '')
print()
print(f"Total students:   {total_students_num:,}")
print(f"Total tuition   $ {total_tuition:,}")
print()
print()
print(f"Student mean:     {mean_students_num:,.2f}")
print(f"Student median:   {median_students_num:,}")
print()
print()
print(f"Tuition mean:   $ {mean_tuition_num:,.2f}")
print(f"Tuition median: $ {median_tuition_num:,}")
print()
for i in range(30):
    print("*", end = '')
