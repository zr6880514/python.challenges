import random

def regional_election(region_chance):
    if random.random() <= region_chance:
        return "win"
    else:
        return "loss"

region1_chance = .87
region2_chance = .65
region3_chance = .17

num_trial = 10_000
wins = 0

for trial in range(num_trial):
    region1_outcome = regional_election(region1_chance)
    region2_outcome = regional_election(region2_chance)
    region3_outcome = regional_election(region3_chance)
    if region1_outcome == "win":
        if region2_outcome == "win":
            wins = wins + 1
            continue
        elif region3_outcome == "win":
            wins = wins + 1
            continue
    elif region2_outcome == "win" and region3_outcome == "win":
        wins = wins + 1

total_election_chance = wins / num_trial
print(f"Candidate A wins in {total_election_chance:.2%} of times.")
