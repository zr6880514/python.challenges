nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
         "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes",
         "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant",
              "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in",
                "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously",
           "sensuously"]

import random

noun1 = random.choice(nouns)
noun2 = random.choice(nouns)
noun3 = random.choice(nouns)
verb1 = random.choice(verbs)
verb2 = random.choice(verbs)
verb3 = random.choice(verbs)
adj1 = random.choice(adjectives)
adj2 = random.choice(adjectives)
adj3 = random.choice(adjectives)
prep1 = random.choice(prepositions)
prep2 = random.choice(prepositions)
adverb1 = random.choice(adverbs)

vowels = list("aeiou")

def indefinite_article(adj, vowels_list):
    '''"An" if following adjective begins with vowel, otherwise "A"'''
    for letter in vowels_list:
        if adj[0].lower() == letter:
            return "An"
    return "A"

print(f"{indefinite_article(adj1, vowels)} {adj1} {noun1}")
print()
print(f"{indefinite_article(adj1, vowels)} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}")
print(f"{adverb1}, the {noun1} {verb2}")
print(f"the {noun2} {verb3} {prep2} {(indefinite_article(adj3, vowels)).lower()} {adj3} {noun3}")

